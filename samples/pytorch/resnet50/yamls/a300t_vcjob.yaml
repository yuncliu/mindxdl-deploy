apiVersion: v1
kind: ConfigMap
metadata:
  name: rings-config-mindx-dls-test     # The value of JobName must be the same as the name attribute of the following job. The prefix rings-config- cannot be modified.
  namespace: vcjob                      # Select a proper namespace based on the site requirements. (The namespaces of ConfigMap and Job must be the same. In addition, if the tjm component of MindX-add exists, the vcjob namespace cannot be used.)
  labels:
    ring-controller.atlas: ascend-910
data:
  hccl.json: |
    {
        "status":"initializing"
    }
---
apiVersion: batch.volcano.sh/v1alpha1
kind: Job
metadata:
  name: mindx-dls-test                  # The value must be consistent with the name of ConfigMap.
  namespace: vcjob                      # Select a proper namespace based on the site requirements. (The namespaces of ConfigMap and Job must be the same. In addition, if the tjm component of MindX-add exists, the vcjob namespace cannot be used.)
  labels:
    ring-controller.atlas: ascend-910   # The HCCL-Controller distinguishes Ascend 910 and other processors based on this label.
spec:
  minAvailable: 1                       # The value of minAvailable is 1 in a single-node scenario and N in an N-node distributed scenario.
  schedulerName: volcano                # Use the Volcano scheduler to schedule jobs.
  policies:
    - event: PodEvicted
      action: RestartJob
  plugins:
    ssh: []
    env: []
    svc: []
  maxRetry: 3
  queue: default
  tasks:
  - name: "default-test"
    replicas: 1                         # The value of replicas is 1 in a single-node scenario and N in an N-node scenario. The number of NPUs in the requests field is 8 in an N-node scenario.
    template:
      metadata:
        labels:
          app: tf
          ring-controller.atlas: ascend-910
      spec:
        hostNetwork: true
        containers:
        - image: torch:b035             # Training framework image, which can be modified.
          imagePullPolicy: IfNotPresent
          name: torch
          env:
          - name: XDL_IP                # IP address of the physical node, which is used to identify the node where the pod is running
            valueFrom:
              fieldRef:
                fieldPath: status.hostIP
          - name: mindx-dls-test        # The value must be the same as that of Jobname.
            valueFrom:
              fieldRef:
                fieldPath: metadata.name
          - name: MY_POD_IP
            valueFrom:
              fieldRef:
                fieldPath: status.podIP
          command:
          - "/bin/bash"
          - "-c"
          - "cd /job/code/train; bash train_start.sh"         # Command for running the training script
          #args: [ "while true; do sleep 30000; done;"  ]     # Comment out the preceding line and enable this line. You can manually run the training script in the container to facilitate debugging.
                                                              # The command is 'kubectl exec -it -n {namespace} {podname} bash'
          resources:
            requests:
              huawei.com/Ascend910: 1                         # Number of required NPUs. The maximum value is 2. You can add lines below to configure resources such as memory and CPU.
            limits:
              huawei.com/Ascend910: 1                         # The value must be consistent with that in requests.
          volumeMounts:
          - name: ascend-910-config
            mountPath: /user/serverid/devindex/config
          - name: code
            mountPath: /job/code/
          - name: data
            mountPath: /job/data
          - name: output
            mountPath: /job/output
          - name: ascend-driver
            mountPath: /usr/local/Ascend/driver
          - name: ascend-add-ons
            mountPath: /usr/local/Ascend/add-ons
          - name: dshm
            mountPath: /dev/shm
          - name: localtime
            mountPath: /etc/localtime
        nodeSelector:
          host-arch: huawei-arm                                         # Configure the label based on the actual job.
          accelerator-type: card                                        # servers (with Atlas 300T training cards)
        volumes:
        - name: ascend-910-config
          configMap:
            name: rings-config-mindx-dls-test                           # Correspond to the ConfigMap name above.
        - name: code
          nfs:
            server: 127.0.0.1                                           # IP address of the NFS server. In this example, the shared path is /data/atlas_dls/.
            path: "/data/atlas_dls/code/ResNet50_for_PyTorch"           # Configure the path of the training script. Modify the path based on the actual script name and path.
        - name: data
          nfs:
            server: 127.0.0.1                                           # IP address of the NFS server
            path: "/data/atlas_dls/public/dataset/resnet50/dataset_PT"  # Configure the path of the training set.
        - name: output
          nfs:
            server: 127.0.0.1                                           # IP address of the NFS server
            path: "/data/atlas_dls/output/"                             # Configure the path for saving the configuration model, which is related to the script.
        - name: ascend-driver
          hostPath:
            path: /usr/local/Ascend/driver                              # Configure the NPU driver and mount it to Docker.
        - name: ascend-add-ons
          hostPath:
            path: /usr/local/Ascend/add-ons                             # Configure the add-ons driver of the NPU and mount it to Docker.
        - name: dshm
          emptyDir:
            medium: Memory
        - name: localtime
          hostPath:
            path: /etc/localtime                                        # Configure the Docker time.
        restartPolicy: OnFailure